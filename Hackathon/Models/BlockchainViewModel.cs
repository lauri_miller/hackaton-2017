﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Hackathon.Models
{
    public class BlockchainViewModel
    {
        public IEnumerable<BlockViewModel> Blocks { get; set; }
    }

    public class BlockViewModel
    {
        public string Previous { get; set; }
        public string Nonce{ get; set; }
        public int BlockNr { get; set; }
        public string Data { get; set; } = "";
        public string Hash { get; set; }

        public bool IsValid
        {
            get
            {
                if (Hash == null) return false;
                return Hash.Substring(0, 4) == "0000";
            }
        }
    }
}