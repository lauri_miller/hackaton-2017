﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.Mvc;
using Hackathon.Models;

namespace Hackathon.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View(new BlockchainViewModel() {Blocks = new []{new BlockViewModel () {BlockNr = 1}, new BlockViewModel() {BlockNr = 2}, new BlockViewModel() { BlockNr = 3 } } });
        }
        [HttpPost]

        public ActionResult Index(BlockViewModel block)
        {
            var result = Miner.Mine(block.Data);
            block.Hash = result.Hash;
            block.Nonce = result.Nonce;
                
            return View(new BlockchainViewModel() {Blocks = new [] {block}});
        }
        [HttpGet]
        public ActionResult UpdatePartial(string data, string nonce, string previous)
        {
            var hash = Miner.GetHashSha256(data + previous + nonce);
            return PartialView("__Valid", new BlockViewModel {Hash = hash, Nonce = nonce});
        }
        [HttpGet]
        public ActionResult MinePartial(string data, string previous)
        {
            var result = Miner.Mine(data+previous);
            return PartialView("__Valid", new BlockViewModel { Hash = result.Hash, Nonce = result.Nonce});
        }
    }

    public class BlockchainViewModelProvider
    {
        public BlockchainViewModel Provide()
        {
            throw new NotImplementedException();
        }
    }
    public class Miner
    {
        public static MinerViewModel Mine(string data)
        {
            var nonce = 0;
            var hash = GetHashSha256(data);
            var resultingNonce = "";
            while (hash.Substring(0, 4) != "0000")
            {
                hash = GetHashSha256(data + nonce);
                resultingNonce = nonce.ToString();
                nonce++;

                if (nonce > 700000)
                {
                    hash = "Timed out.";
                }
            }
            return new MinerViewModel
            {
                Nonce = resultingNonce,
                Hash = hash
            };
        }
        public static string GetHashSha256(string text)
        {
            var bytes = Encoding.Unicode.GetBytes(text);
            var hashstring = new SHA256Managed();
            var hash = hashstring.ComputeHash(bytes);
            return hash.Aggregate(string.Empty, (current, x) => current + $"{x:x2}");
        }
    }

    public class MinerViewModel
    {
        public string Nonce { get; set; }
        public string Hash { get; set; }
    }
}